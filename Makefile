default:
	@echo
	@echo "  make dev  -- increases inotify limit and starts HTTP server"
	@echo

dev:
	echo 999999 | sudo tee -a /proc/sys/fs/inotify/max_user_watches
	ember s
