rm public/tmp/view.png -f
rm public/tmp/amplified.png -f
fswebcam --png 0 --no-banner -S 1 -r 1024x768 public/tmp/view.png
convert public/tmp/view.png -channel R -evaluate set 0 +channel -level 70%,100% -morphology Open Disk public/tmp/amplified.png

#~ convert public/tmp/amplified.png -negate -background none -transparent white public/tmp/amplified.png
