color=`convert public/tmp/amplified.png -crop 1x1+$1+$2 txt:- | awk '{print $4}' | tail -n 1`

if [ "${color}" != "black" ]; then
  echo "on"
else
  echo "off"
fi
