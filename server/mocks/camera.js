/*jshint node:true*/

var execFile = require('child_process').execFile
var async = require('async')

var options = {
  config_location: "public/tmp/config.json"
}

function shot(req, res, next, cb) {
  
  var child = execFile('bash',  'scripts/take.sh'.split(' '), function(err, stdout, stderr) {
    if (err) {
      res.sendStatus(503);
    }
    else {
      cb(req, res, next);
    }
  });
}

var fs = require('fs');

module.exports = function(app) {
  var express = require('express');
  var cameraRouter = express.Router();

  cameraRouter.get('/hello', function(req, res, next) {
    res.sendStatus(200);
  });

  cameraRouter.get('/shot', function(req, res, next) {
    shot(req, res, next, function() {
      res.json([]);
    })
  });

  cameraRouter.get('/config', function(req, res) {
    try {
      fs.accessSync(options.config_location, fs.F_OK);
      var json = JSON.parse(fs.readFileSync(options.config_location, 'utf-8'));
      res.json(json);
    } catch (e) {
      res.json({leds: []});
    }
  });

  cameraRouter.post('/config', function(req, res) {
    var str = JSON.stringify(req.body);
    fs.writeFileSync(options.config_location, str, 'utf-8');
    res.status(200).end();
  });

  cameraRouter.get('/leds', function(req, res, next) {
    
    shot(req, res, next, function() {
      var config = {}
      
      try {
        fs.accessSync(options.config_location, fs.F_OK);
        config = JSON.parse(fs.readFileSync(options.config_location, 'utf-8'));
      } catch (e) {
        res.sendStatus(503);
        return;
      }
      
      var coords = [];
      for (key in config['leds']) {
        coords.push({"x": config['leds'][key].x, "y": config['leds'][key].y});
      }
      
      async.map(coords, function(tuple, cb) {
        execFile('bash',  ("scripts/check.sh "+ tuple.x +" "+ tuple.y).split(' '), function(err, stdout, stderr) {
          cb(err, stdout.trim());
        });
      },
      function(err, results) {
        for (key in results) {
          config['leds'][key]['value'] = (results[key] == "on");
        }
        res.json(config);
      });
    });
  
  });

  app.use('/api/camera', require('body-parser').json());
  app.use('/api/camera', cameraRouter);
};
