import Ember from 'ember';

export default Ember.Component.extend({
  active: Ember.computed('activeLight', 'light', function() {
    return this.get('activeLight') === this.get('light');
  }),
  
  changeObserver: Ember.observer('light.x', 'light.y', 'light.name', 'light.type', function() {
    this.get('save')(this.get('light'));
  }),
  
  actions: {
    remove: function() {
      this.get('remove')();
    },
    select: function() {
      this.get('select')();
    }
  }
});
