import Ember from 'ember';

export default Ember.Component.extend({
  click: function(ev) {
    let x = ev.pageX - Ember.$(ev.target).offset().left - parseInt(Ember.$(ev.target).css("border-left-width"));
    let y = ev.pageY - Ember.$(ev.target).offset().top  - parseInt(Ember.$(ev.target).css("border-top-width"));
    this.get('selected')(x, y);
  }
});
