import Ember from 'ember';

export default Ember.Component.extend({
  classNameBindings: ['active'],
  
  active: Ember.computed('activeSpot', 'entry', function() {
    return this.get('activeSpot') === this.get('entry');
  })
});
