import Ember from 'ember';

export default Ember.Route.extend({
  model: function() {
    return this.get('store').findAll('light');
  },
  setupController: function(controller, lights) {
    this._super(controller, lights);
    if (lights.get('length') === 0) {
      this.controllerFor('application').set('expanded', true);
    }
  },
  beforeModel(transition) {
    if (transition.targetName === 'index') { 
      this.transitionTo('shot', {
        queryParams: {
          random: Math.random()
        }
      });
    }
  }
});
