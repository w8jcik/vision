import Ember from 'ember';

export default Ember.Route.extend({
  actions: {
    loading() {
      this.controllerFor('application').set('loading', true);
      return true;
    },
    didTransition() {
      this.controllerFor('application').set('loading', false);
      this.controllerFor('application').set('random', Math.random());
    }
  },
  model: function() {
    return new Ember.RSVP.hash({
      data: Ember.$.getJSON('/api/camera/shot')
    });
  },
  queryParams: {
    random: {
      refreshModel: true
    }
  }
});
