import Ember from 'ember';

export default Ember.Route.extend({
  actions: {
    loading() {
      this.controllerFor('application').set('loading', true);
      return true;
    },
    didTransition() {
      this.controllerFor('application').set('loading', false);
    }
  }
});
