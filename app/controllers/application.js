import Ember from 'ember';

export default Ember.Controller.extend({
  expanded: false,
  
  switched: false,
  
  active: undefined,
  
  loading: false,
  
  loadingLeds: false,
  
  random: undefined,
  
  newFocused: Ember.computed('active', function() {
    return this.get('active') === undefined;
  }),
    
  actions: {
    selectLight: function(light) {
      this.set('active', light);
    },
    deselect: function() {
      this.set('active', undefined);
    },
    saveCoordinates: function(x, y) {
      
      let selected = this.get('active');
      
      if (selected) {
        selected.set('x', x);
        selected.set('y', y);
        selected.save();
      }
      else {
      
        var light = this.store.createRecord('light', {
          x: x,
          y: y,
          name: this.get('name'),
          type: this.get('type')
        });
        
        light.save();
        
        this.set('x', '');
        this.set('y', '');
        this.set('name', '');
      }
      
      let that = this;
      let justOneMore = false;
      let done = false;
      
      this.get('model').forEach(function(entry) {
        if (done) { return; }
        
        if (justOneMore) {
          that.set('active', entry);
          done = true;
        }
        
        if (that.get('active') && that.get('active') === entry) {
          justOneMore = true;
        }
      });
      
      if (!done) {
        this.send('deselect');
        Ember.$("#newEntryNameInput").trigger('focus');
      }
      
    },
    save: function(light) {
      light.save();
    },
    removeLight: function(light) {
      if (this.get('active') === light) {
        this.set('active', undefined);
      }
      light.destroyRecord();
    },
    toggle: function() {
      this.toggleProperty('expanded');
    },
    switch: function() {
      this.toggleProperty('switched');
    },
    saveAll: function() {
      let config = [];
      this.get('model').forEach(function(entry) {
        
        let name = entry.get('name');
        let type = entry.get('type');
        if (name == null) {
          name = "";
        }
        if (type == null) {
          type = "";
        }
        
        config.push({
          x: parseInt(entry.get('x')),
          y: parseInt(entry.get('y')),
          name: ""+ name,
          type: ""+ type
        });
      });
      let data = { "leds": config };
      
      Ember.$.ajax('/api/camera/config', {
        type: 'POST',
        dataType: 'JSON',
        data: JSON.stringify(data),
        contentType: "application/json",
        success: function(data) {
          return data;
        },
        error: function (jqXHR) {
          window.console.log(jqXHR);
        }
      });
      
    },
    loadAll: function() {
      this.set('loadingLeds', true); 
      
      Ember.$.ajax('/api/camera/config', {
        type: 'GET',
        dataType: 'JSON',
        contentType: "application/json",
        success: (data) => {
          for (var entry of data.leds) {
            this.get('store').createRecord('light', entry).save();
          }
          this.get('model').forEach(function(entry) {
            entry.destroyRecord();
          });
          this.set('loadingLeds', false);
        },
        error: function (jqXHR) {
          console.log('loading leds failed', jqXHR);
          this.set('loadingLeds', false);
        }
      });
      
    }
  }
});
