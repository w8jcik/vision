import Ember from 'ember';

export default Ember.Controller.extend({
  cameraConstraints: {
    video: {
      width: 1280
    },
    audio: false
  }
});
