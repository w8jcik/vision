import DS from 'ember-data';

export default DS.Model.extend({
  x: DS.attr('number'),
  y: DS.attr('number'),
  name: DS.attr('string'),
  type: DS.attr('string'),
  comment: DS.attr('string')
});
