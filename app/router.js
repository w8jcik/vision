import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('shot', { queryParams: ['random'] });
  this.route('amplified', { queryParams: ['random'] });
  this.route('live');
});

export default Router;
