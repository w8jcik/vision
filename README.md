# Vision

Uses camera to get state of the leds.

![taken](https://bitbucket.org/w8jcik/vision/raw/master/docs/taken.png)

Applies a set of ImageMagick filters to extract which leds are on.

![amplified](https://bitbucket.org/w8jcik/vision/raw/master/docs/amplified.png)

Position and name of each led can be specified using a GUI.

![config](https://bitbucket.org/w8jcik/vision/raw/master/docs/config.png)

Exposes JSON API

```
/api/camera/leds

{
  "leds": [
    {
      "x": 139,
      "y": 107,
      "name": "main_valve_ac",
      "type": "230V",
      "value": false
    },
    {
      "x": 126,
      "y": 187,
      "name": "fan",
      "type": "230V",
      "value": true
    },
    {
      "x": 114,
      "y": 267,
      "name": "_unused",
      "type": "230V",
      "value": false
    },
    ...
  ]
}
```

It's purpose is to provide feedback for automated tests.

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with NPM)
* [Bower](http://bower.io/)
* [Ember CLI](http://ember-cli.com/)
* [Watchman](https://facebook.github.io/watchman/)
* [ImageMagick](www.imagemagick.org)
* [fswebcam](http://www.firestorm.cx/fswebcam)

Skipping Watchman results in a silent failure.

## Installation

* `git clone git@bitbucket.org:w8jcik/vision.git` this repository
* `cd vision`
* `npm install`
* `bower install`

## Optional patches ##

```diff
node_modules/ember-cli-usermedia/addon/components/user-media-src.js`

- tagName: '',
+ tagName: 'div',

+++
classNameBindings: ['_gotError:error'],

_gotError: function () {
  return this.get('error') !== null
}.property('error'),
+++
```

## Running / Development

* `ember serve`
* Visit the app at [http://localhost:4200](http://localhost:4200).

![config](https://bitbucket.org/w8jcik/vision/raw/master/docs/vision.png)

## Browser limitations ##

Vertical centering uses `display: flex`.

Alternative centering methods:

http://stackoverflow.com/questions/6490252/vertically-centering-a-div-inside-another-div

Firefox doesn't have an API to shutdown camera. User has to do it manually.

*ember-cli-usermedia* doesn't work with current Google Chrome. You won't get *live* preview.


## Licenses ##

Camera icon comes from http://unrestrictedstock.com/projects/camera-icons-free-stock-vector-set/